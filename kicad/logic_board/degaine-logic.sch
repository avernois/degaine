EESchema Schematic File Version 4
LIBS:degaine-logic-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L crafting_labs:Arduino_nano U1
U 1 1 5AD9D2A5
P 7300 3150
F 0 "U1" H 7600 2250 60  0000 C CNN
F 1 "Arduino_nano" H 7050 2250 60  0000 C CNN
F 2 "Module:Arduino_Nano" H 6550 2450 60  0001 C CNN
F 3 "" H 6550 2450 60  0000 C CNN
	1    7300 3150
	1    0    0    -1  
$EndComp
$Comp
L degaine-logic-rescue:USB_OTG-RESCUE-degaine-logic P2
U 1 1 5AD9D312
P 9250 3500
F 0 "P2" H 9575 3375 50  0000 C CNN
F 1 "USB_OTG" H 9250 3700 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" V 9200 3400 50  0001 C CNN
F 3 "" V 9200 3400 50  0000 C CNN
	1    9250 3500
	0    1    1    0   
$EndComp
$Comp
L degaine-logic-rescue:CONN_01X03 P1
U 1 1 5AD9D3FF
P 6650 1150
F 0 "P1" H 6650 1350 50  0000 C CNN
F 1 "CONN_01X03" V 6750 1150 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 6650 1150 50  0001 C CNN
F 3 "" H 6650 1150 50  0000 C CNN
	1    6650 1150
	0    -1   -1   0   
$EndComp
$Comp
L degaine-logic-rescue:R R1
U 1 1 5AD9D504
P 5450 2000
F 0 "R1" V 5530 2000 50  0000 C CNN
F 1 "R" V 5450 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5380 2000 50  0001 C CNN
F 3 "" H 5450 2000 50  0000 C CNN
	1    5450 2000
	1    0    0    -1  
$EndComp
$Comp
L degaine-logic-rescue:R R2
U 1 1 5AD9D588
P 5700 2000
F 0 "R2" V 5780 2000 50  0000 C CNN
F 1 "R" V 5700 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5630 2000 50  0001 C CNN
F 3 "" H 5700 2000 50  0000 C CNN
	1    5700 2000
	1    0    0    -1  
$EndComp
$Comp
L degaine-logic-rescue:R R3
U 1 1 5AD9D5B6
P 5950 2000
F 0 "R3" V 6030 2000 50  0000 C CNN
F 1 "R" V 5950 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5880 2000 50  0001 C CNN
F 3 "" H 5950 2000 50  0000 C CNN
	1    5950 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 5AD9E48F
P 8800 2500
F 0 "#PWR01" H 8800 2350 50  0001 C CNN
F 1 "+5V" H 8800 2640 50  0000 C CNN
F 2 "" H 8800 2500 50  0000 C CNN
F 3 "" H 8800 2500 50  0000 C CNN
	1    8800 2500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5AD9E4B1
P 8800 2800
F 0 "#PWR02" H 8800 2550 50  0001 C CNN
F 1 "GND" H 8800 2650 50  0000 C CNN
F 2 "" H 8800 2800 50  0000 C CNN
F 3 "" H 8800 2800 50  0000 C CNN
	1    8800 2800
	0    1    1    0   
$EndComp
Text GLabel 9200 2500 2    60   Input ~ 0
5V
Text GLabel 9200 2800 2    60   Input ~ 0
GND
Wire Wire Line
	8800 2500 9200 2500
Wire Wire Line
	9200 2800 9000 2800
Text GLabel 8750 3700 0    60   Input ~ 0
GND
Text GLabel 8750 3300 0    60   Input ~ 0
5V
Wire Wire Line
	8750 3300 8950 3300
Wire Wire Line
	8950 3700 8750 3700
NoConn ~ 8950 3400
NoConn ~ 8950 3500
NoConn ~ 8950 3600
NoConn ~ 9350 3900
Text GLabel 8050 2800 2    60   Input ~ 0
5V
Text GLabel 8050 2600 2    60   Input ~ 0
GND
Wire Wire Line
	8050 2600 7800 2600
Wire Wire Line
	7800 2800 8050 2800
Text GLabel 4300 3150 0    60   Input ~ 0
GND
Wire Wire Line
	4700 3150 4300 3150
Wire Wire Line
	4700 2450 5450 2450
Wire Wire Line
	6550 2450 6550 3000
Wire Wire Line
	6550 3000 6800 3000
Wire Wire Line
	5500 2750 5700 2750
Wire Wire Line
	6400 2750 6400 3100
Wire Wire Line
	6400 3100 6800 3100
Wire Wire Line
	5500 3150 5950 3150
Wire Wire Line
	6250 3150 6250 3200
Wire Wire Line
	6250 3200 6800 3200
Wire Wire Line
	6650 1350 6650 2900
Wire Wire Line
	6650 2900 6800 2900
Text GLabel 6750 1500 3    60   Input ~ 0
GND
Text GLabel 6550 1500 3    60   Input ~ 0
5V
Wire Wire Line
	6550 1500 6550 1350
Wire Wire Line
	6750 1350 6750 1500
NoConn ~ 6800 2500
NoConn ~ 6800 2600
NoConn ~ 6800 2700
NoConn ~ 6800 2800
NoConn ~ 6800 3300
NoConn ~ 6800 3400
NoConn ~ 6800 3500
NoConn ~ 6800 3600
NoConn ~ 6800 3700
NoConn ~ 6800 3800
NoConn ~ 6800 3900
NoConn ~ 7800 3900
NoConn ~ 7800 3800
NoConn ~ 7800 3700
NoConn ~ 7800 3600
NoConn ~ 7800 3500
NoConn ~ 7800 3400
NoConn ~ 7800 3300
NoConn ~ 7800 3200
NoConn ~ 7800 3100
NoConn ~ 7800 3000
NoConn ~ 7800 2900
NoConn ~ 7800 2700
NoConn ~ 7800 2500
Text GLabel 5300 1650 0    60   Input ~ 0
5V
Wire Wire Line
	5300 1650 5450 1650
Wire Wire Line
	5450 1650 5450 1850
Wire Wire Line
	5700 1650 5700 1850
Connection ~ 5450 1650
Wire Wire Line
	5950 1650 5950 1850
Connection ~ 5700 1650
Wire Wire Line
	5450 2150 5450 2450
Connection ~ 5450 2450
Wire Wire Line
	5700 2150 5700 2750
Connection ~ 5700 2750
Wire Wire Line
	5950 2150 5950 3150
Connection ~ 5950 3150
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5ADA03BB
P 9000 2950
F 0 "#FLG03" H 9000 3045 50  0001 C CNN
F 1 "PWR_FLAG" H 9000 3130 50  0000 C CNN
F 2 "" H 9000 2950 50  0000 C CNN
F 3 "" H 9000 2950 50  0000 C CNN
	1    9000 2950
	-1   0    0    1   
$EndComp
Wire Wire Line
	9000 2950 9000 2800
Connection ~ 9000 2800
Wire Wire Line
	5450 1650 5700 1650
Wire Wire Line
	5700 1650 5950 1650
Wire Wire Line
	5450 2450 6550 2450
Wire Wire Line
	5700 2750 6400 2750
Wire Wire Line
	5950 3150 6250 3150
Wire Wire Line
	9000 2800 8800 2800
$Comp
L Device:Rotary_Encoder_Switch SW2
U 1 1 5C040495
P 5100 2950
F 0 "SW2" H 5100 2583 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 5100 2674 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E-Switch_Vertical_H20mm_CircularMountingHoles" H 4950 3110 50  0001 C CNN
F 3 "~" H 5100 3210 50  0001 C CNN
	1    5100 2950
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 2850 4800 2850
Wire Wire Line
	4700 2450 4700 2850
Wire Wire Line
	4700 3150 4700 3050
Wire Wire Line
	4700 3050 4800 3050
Wire Wire Line
	5400 2950 5850 2950
Wire Wire Line
	5500 2750 5500 2850
Wire Wire Line
	5500 2850 5400 2850
Wire Wire Line
	5500 3150 5500 3050
Wire Wire Line
	5500 3050 5400 3050
Text GLabel 5850 2950 2    60   Input ~ 0
GND
$EndSCHEMATC
