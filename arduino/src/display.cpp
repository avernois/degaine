#include "display.h"

#define NUM_LEDS 28
#define DATA_PIN 2

CRGB leds[NUM_LEDS];

int symbols[38][7] = {  { 1,1,1,1,1,1,0 },    // 0
                        { 0,1,1,0,0,0,0 },    // 1
                        { 1,1,0,1,1,0,1 },    // 2
                        { 1,1,1,1,0,0,1 },    // 3
                        { 0,1,1,0,0,1,1 },    // 4
                        { 1,0,1,1,0,1,1 },    // 5
                        { 1,0,1,1,1,1,1 },    // 6
                        { 1,1,1,0,0,0,0 },    // 7
                        { 1,1,1,1,1,1,1 },    // 8
                        { 1,1,1,1,0,1,1 },    // 9
                        { 1,1,1,0,1,1,1 },    // A
                        { 0,0,1,1,1,1,1 },    // B
                        { 1,0,0,1,1,1,0 },    // C
                        { 0,1,1,1,1,0,1 },    // D
                        { 1,0,0,1,1,1,1 },    // E
                        { 1,0,0,0,1,1,1 },    // F
                        { 1,1,1,1,0,1,1 },    // G
                        { 0,1,1,0,1,1,1 },    // H
                        { 0,0,0,0,1,1,0 },    // I
                        { 0,1,1,1,1,0,0 },    // J
                        { 0,1,1,0,1,1,1 },    // K
                        { 0,0,0,1,1,1,0 },    // L
                        { 1,0,1,0,1,0,0 },    // M
                        { 0,0,1,0,1,0,1 },    // N
                        { 1,1,1,1,1,1,0 },    // O
                        { 1,1,0,0,1,1,1 },    // P
                        { 1,1,1,0,0,1,1 },    // Q
                        { 0,0,0,0,1,0,1 },    // R
                        { 1,0,1,1,0,1,1 },    // S
                        { 0,0,0,1,1,1,1 },    // T
                        { 0,1,1,1,1,1,0 },    // U
                        { 0,0,1,1,1,0,0 },    // V
                        { 0,1,0,1,0,1,0 },    // W
                        { 0,1,1,0,1,1,1 },    // X
                        { 0,1,1,1,0,1,1 },    // Y
                        { 1,1,0,1,1,0,1 },    // Z
                        { 0,0,0,0,0,0,0 },    // SPACE
                        { 0,0,0,0,0,0,1 }     // DASH
                };

void addSegments(const int* segments, const CRGB color, const int offset);
int printableNumber(int number);

Display::Display() {
    FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
}

CRGB adjustedColor(CRGB color, int power_in_mA) {
    if(power_in_mA == 0)
        return color;

    float coeff = (color.r + color.g + color.b) * 20./255 * 28;
    coeff = (power_in_mA*100.)/coeff;

    if(coeff > 100)
        return color;

    return CRGB(color.r * coeff / 100, color.g * coeff / 100, color.b * coeff / 100);
}

void Display::print(const Symbol symbol1, const Symbol symbol2, const Face face, const CRGB color, const int powerLimit) {
    FastLED.clear(false);
    CRGB adjusted = adjustedColor(color, powerLimit);

    switch (face) {
    case Display::FACE_A:
        addSegments(symbols[symbol1], adjusted, 0);
        addSegments(symbols[symbol2], adjusted, 7);
        break;

    case Display::FACE_B:
        addSegments(symbols[symbol1], adjusted, 14);
        addSegments(symbols[symbol2], adjusted, 21);
        break;
        
    case Display::BOTH_FACE:
        addSegments(symbols[symbol1], adjusted, 0);
        addSegments(symbols[symbol2], adjusted, 7);
        addSegments(symbols[symbol1], adjusted, 14);
        addSegments(symbols[symbol2], adjusted, 21);
        break;

    default:
        break;
    }

    FastLED.show();
}

void Display::print(const int number, const Face face, const CRGB color, const int powerLimit = 0) {
    int toPrint = printableNumber(number);
    int decade = toPrint/10 ;
    int unit = toPrint%10;
   
    if (toPrint >= 0)
        print((Symbol) decade, (Symbol) unit, face, color, powerLimit);
    else {
        print(Display::DASH, (Symbol) abs(unit), face, color, powerLimit);
    }   

}

void Display::printBothFace(const int number, const CRGB color){
    print(number, BOTH_FACE, color);
}

void addSegments(const int* segments, const CRGB color, const int offset) {
    for(int segment = 0; segment < 7; segment ++) {
        if(segments[segment] == 1 ) {
            leds[segment + offset] = color;
        }
    }
}

int printableNumber(int number) {
    if (number < -9)  return -9;
    if (number > 99) return 99;
    
    return number;
}