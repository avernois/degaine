#ifndef __have__Display_h__
#define __have__Display_h__

#include <FastLED.h>

class Display {

    public:
  
    typedef enum { FACE_A, FACE_B, BOTH_FACE } Face;
    typedef enum { N_0 = 0, N_1, N_2, N_3, N_4, N_5, N_6, N_7, N_8, N_9,
                   A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
                   SPACE,
                   DASH
                 } Symbol;

    Display();

    void printBothFace(const int number, const CRGB color);
    void print(const int number, const Face face, const CRGB color, const int powerLimit = 0);
    void print(const Symbol symbol1, const Symbol symbol2, const Face face, const CRGB color, const int powerLimit = 0);
};
#endif