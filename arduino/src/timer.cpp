#include "timer.h"

typedef enum { RUNNING, STOPPED, PAUSED, ENDED, REALLY_ENDED, STANDBY, CONFIGURING_FACE} State;
const int STANDBY_DELAY = 2*60;

const int FIRST_TRANSITION_PERCENT = 50;
const int SECOND_TRANSITION_PERCENT = 10;
const int DISPLAY_TO_SECOND_LIMIT = 99;

typedef struct {
    State state;
    Display::Face face;
    int16_t duration;
    int powerLimit;
    uint16_t pulse;
} CurrentState;

CurrentState currentState = { STOPPED, Display::BOTH_FACE, 60, 500, 0 };

void updateDisplayWhenEnded(Display *display, const CurrentState state, Chrono *chrono, Timer::Colors colors);
void updateStateWhenEnded(CurrentState *state, ClickEncoder *encoder, Chrono *chrono);
void updateDisplayWhenReallyEnded(Display *display, const CurrentState state, Timer::Colors colors);
void updateStateWhenReallyEnded(CurrentState *state, ClickEncoder *encoder, Chrono *chrono);
void updateDisplayWhenStopped(Display *display, const CurrentState state, Timer::Colors colors);
void updateStateWhenStopped(CurrentState *state, ClickEncoder *encoder, Chrono *chrono);
void enterStopped(CurrentState *state, ClickEncoder *encoder, Chrono *chrono);
void updateDisplayWhenStandby(Display *display, const CurrentState state, Chrono *chrono, Timer::Colors colors);
void updateStateWhenStandby(CurrentState *state, ClickEncoder *encoder, Chrono *chrono);
void updateDisplayWhenPaused(Display *display, const CurrentState state, Chrono *chrono, Timer::Colors colors);
void updateStateWhenPaused(CurrentState *state, ClickEncoder *encoder, Chrono *chrono);
void updateDisplayWhenRunning(Display *display, const CurrentState state, Chrono *chrono, Timer::Colors colors);
void updateStateWhenRunning(CurrentState *state, ClickEncoder *encoder, Chrono *chrono);
void updateDisplayWhenConfiguringFace(Display *display, const CurrentState state, Timer::Colors colors);
void updateStateWhenConfiguringFace(CurrentState *state, ClickEncoder *encoder, Chrono *chrono);

Timer::Timer(Display *display, ClickEncoder *encoder, Chrono *chrono) {
    this->display = display;
    this->encoder = encoder;
    this->chrono = chrono;
}

void Timer::update() {
    switch (currentState.state) {
    case ENDED:
        updateDisplayWhenEnded(display, currentState, chrono, colors);
        updateStateWhenEnded(&currentState, encoder, chrono);
        break;

    case REALLY_ENDED:
        updateDisplayWhenReallyEnded(display, currentState, colors);
        updateStateWhenReallyEnded(&currentState, encoder, chrono);
        break;

    case STOPPED:
        updateDisplayWhenStopped(display, currentState, colors);
        updateStateWhenStopped(&currentState, encoder, chrono);
        break;

    case STANDBY:
        updateDisplayWhenStandby(display, currentState, chrono, colors);
        updateStateWhenStandby(&currentState, encoder, chrono);
        break;

    case PAUSED:
        updateDisplayWhenPaused(display, currentState, chrono, colors);
        updateStateWhenPaused(&currentState, encoder, chrono);
        break;

    case RUNNING:
        updateDisplayWhenRunning(display, currentState, chrono, colors);
        updateStateWhenRunning(&currentState, encoder, chrono);
        break;

    case CONFIGURING_FACE:
        updateDisplayWhenConfiguringFace(display, currentState, colors);
        updateStateWhenConfiguringFace(&currentState, encoder, chrono);
        break;
  }
}

int numberToDisplay(int remainingInSeconds) {
    if (remainingInSeconds > DISPLAY_TO_SECOND_LIMIT)
        return remainingInSeconds/60;
    else
        return remainingInSeconds;
}

CRGB getColorForRemainingPercent(Timer::Colors colors, const float remainingPercent) {
    if (remainingPercent > FIRST_TRANSITION_PERCENT)
        return colors.running_start;

    if (remainingPercent > SECOND_TRANSITION_PERCENT)
        return colors.running_mid;

    if (remainingPercent > 0)   
        return colors.running_end;

    return colors.ended;
}

bool isNextColorTransitionInSeconds(const int duration, const int remaining) {
    if (remaining < DISPLAY_TO_SECOND_LIMIT)
        return true;
    
    if (duration * FIRST_TRANSITION_PERCENT/100 <= DISPLAY_TO_SECOND_LIMIT)
        return true;

    if ( remaining < duration * FIRST_TRANSITION_PERCENT/100 && duration * SECOND_TRANSITION_PERCENT/100 <= DISPLAY_TO_SECOND_LIMIT)
        return true;

    return false;
}

CRGB getColorForNow(Timer::Colors colors, const int duration, const int remaining) {
    float remainingPercentFromSeconds = remaining*100.0 / duration;
    float remainingPercentFromMinutes = ((remaining/60)*100.0) / (duration/60);

    if(isNextColorTransitionInSeconds(duration, remaining))
        return getColorForRemainingPercent(colors, remainingPercentFromSeconds);
    else
        return getColorForRemainingPercent(colors, remainingPercentFromMinutes);
}

void updateDisplayWhenEnded(Display *display, const CurrentState state, Chrono *chrono, Timer::Colors colors) {
    int exceededTime = state.duration*60  - chrono->elapsed();
    exceededTime = exceededTime/60;

    display->print(exceededTime, state.face, colors.ended, state.powerLimit);
}

void updateStateWhenEnded(CurrentState *state, ClickEncoder *encoder, Chrono *chrono) {
    int exceededTime = (state->duration * 60 - chrono->elapsed());
    exceededTime = exceededTime/60;
    if (exceededTime == -9) {
        chrono->stop();
        state->state = REALLY_ENDED;
        return;
    }

    ClickEncoder::Button button = encoder->getButton();
    switch(button) {
    case ClickEncoder::Clicked:
    case ClickEncoder::Released:
        enterStopped(state, encoder, chrono);
        break;
    default:
        break;
    };
}

void updateDisplayWhenReallyEnded(Display *display, const CurrentState state, Timer::Colors colors) {
    display->print(Display::DASH, Display::DASH, state.face, colors.really_ended, state.powerLimit);
}

void updateStateWhenReallyEnded(CurrentState *state, ClickEncoder *encoder, Chrono *chrono) {

    ClickEncoder::Button button = encoder->getButton();
    switch(button) {
    case ClickEncoder::Clicked:
    case ClickEncoder::Released:
        enterStopped(state, encoder, chrono);
        break;
    default:
        break;
    };
}

void updateDisplayWhenStopped(Display *display, const CurrentState state, Timer::Colors colors) {
    display->print(state.duration, state.face, colors.stopped, state.powerLimit);
}

void enterStopped(CurrentState *state, ClickEncoder *encoder, Chrono *chrono) {
    encoder->getValue();// discard any value change while not in STOPPED
    state->state = STOPPED;
    chrono->start();
}

void updateStateWhenStopped(CurrentState *state, ClickEncoder *encoder, Chrono *chrono) {
    int16_t encoderValue = encoder->getValue();
    if (encoderValue !=0) {
        chrono->start(); 
    }

    int16_t newValue = state->duration + encoderValue;

    if (newValue < 0) newValue = 0;

    if (newValue > 99) newValue = 99;

    if (newValue != state->duration) {
        state->duration = newValue;
    }

    ClickEncoder::Button button = encoder->getButton();

    switch(button) {
    case ClickEncoder::Clicked:
        chrono->start();
        state->state = RUNNING;
        break;
    case ClickEncoder::Released:
        state->state = CONFIGURING_FACE;
        break;
    default:
        if(chrono->hasPassed(STANDBY_DELAY)) {
            state->pulse = 0;
            state->state = STANDBY;
        }
        break;
    }
}

void updateDisplayWhenStandby(Display *display, const CurrentState state, Chrono *chrono, Timer::Colors colors) {
    CHSV hsv = rgb2hsv_approximate(colors.standby);
    hsv.value = triwave8(state.pulse/8)/2 + 25;
    display->print(Display::DASH, Display::DASH, state.face, hsv, state.powerLimit);
}

void updateStateWhenStandby(CurrentState *state, ClickEncoder *encoder, Chrono *chrono) {
    state->pulse = (state->pulse + 1);
    
    if (encoder->getValue() !=0) {
        enterStopped(state, encoder, chrono);
    }

    ClickEncoder::Button button = encoder->getButton();

    switch(button) {
    case ClickEncoder::Clicked:
    case ClickEncoder::Released:
        enterStopped(state, encoder, chrono);
        break;
    default:
        break;
    }
}

void updateDisplayWhenPaused(Display *display, const CurrentState state, Chrono *chrono, Timer::Colors colors) {
    display->print(numberToDisplay(state.duration * 60 - chrono->elapsed()), state.face, colors.paused, state.powerLimit);
}

void updateStateWhenPaused(CurrentState *state, ClickEncoder *encoder, Chrono *chrono) {
    ClickEncoder::Button button = encoder->getButton();
    switch(button) {
    case ClickEncoder::Clicked:
        chrono->resume();
        state->state = RUNNING;
        break;
    case ClickEncoder::Released:
        enterStopped(state, encoder, chrono);
        break;
    default:
        break;
    }
}

void updateDisplayWhenRunning(Display *display, const CurrentState state, Chrono *chrono, Timer::Colors colors) {
    int remaining = (state.duration*60  - chrono->elapsed());
    int toDisplay = numberToDisplay(state.duration * 60 - chrono->elapsed());
    CRGB color = getColorForNow(colors, state.duration*60, remaining);

    display->print(toDisplay, state.face, color, state.powerLimit);
}

void updateStateWhenRunning(CurrentState *state, ClickEncoder *encoder, Chrono *chrono) {
    int remaining = (state->duration * 60 - chrono->elapsed());
    if (remaining < 0) {
        state->state = ENDED;
        return;
    }

    ClickEncoder::Button button = encoder->getButton();
    switch(button) {
    case ClickEncoder::Clicked:
        chrono->stop();
        state->state = PAUSED;
        break;
    case ClickEncoder::Released:
        enterStopped(state, encoder, chrono);
        break;
    default:
        break;
    }
}

void updateDisplayWhenConfiguringFace(Display *display, const CurrentState state, Timer::Colors colors) {
    display->print(Display::F, Display::A, state.face, colors.stopped, state.powerLimit);
}

void updateStateWhenConfiguringFace(CurrentState *state, ClickEncoder *encoder, Chrono *chrono) {
    int value = encoder->getValue();
    if(value < 0 && state->face == 0) {
        state->face = Display::BOTH_FACE;
    } else {
        state->face = (Display::Face) ((state->face + value)%3);
    }
    
    ClickEncoder::Button button = encoder->getButton();
    
    switch(button) {
    case ClickEncoder::Released:
    case ClickEncoder::Clicked:
        enterStopped(state, encoder, chrono);
        break;
    default:
        break;
    }
}